from setuptools import setup

setup(
    name="mainpkg",
    version="1.0",
    author="Chenxiong Qi",
    author_email="qcxhome@gmail.com",
    description="Main package of this example",
    license="MIT",
)
