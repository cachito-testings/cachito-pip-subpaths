from setuptools import setup

setup(
    name="pkg2",
    version="0.1",
    author="Chenxiong Qi",
    author_email="qcxhome@gmail.com",
    description="A cool web app",
    license="MIT",
)
