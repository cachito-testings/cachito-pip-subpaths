from setuptools import setup

setup(
    name="pkg1",
    version="0.3",
    author="Chenxiong Qi",
    author_email="qcxhome@gmail.com",
    description="sub package 1",
    license="MIT",
)
